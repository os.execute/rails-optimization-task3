# frozen_string_literal: true

# rake reload_json[fixtures/small.json]
task :reload_json, [:file_name] => :environment do |_task, args|
  json = JSON.parse(File.read(args.file_name))

  ActiveRecord::Base.transaction do
    City.delete_all
    Bus.delete_all
    Service.delete_all
    Trip.delete_all
    ActiveRecord::Base.connection.execute('delete from buses_services;')

    cities = []
    buses = []
    trips = []

    json.each do |trip|
      cities.push(trip['to'], trip['from'])
      buses << trip['bus']
      trips << trip
    end

    cities.uniq!
    buses.uniq!

    cities_index = insert_cities(cities)
    services_index = insert_services
    buses_index = insert_buses(buses)

    insert_bus_service_relations(buses, buses_index, services_index)

    insert_trips(trips, cities_index, buses_index)
  end
end

private

def insert_trips(trips, cities_index, buses_index)
  trip_objects = trips.map do |trip|
    {
      from_id: cities_index[trip['from']],
      to_id: cities_index[trip['to']],
      start_time: trip['start_time'],
      duration_minutes: trip['duration_minutes'],
      price_cents: trip['price_cents'],
      bus_id: buses_index[bus_index(trip['bus'])]
    }
  end

  Trip.insert_all(trip_objects)
end

def insert_cities(cities)
  city_objects = cities.map { |city| { name: city } }
  city_result = City.insert_all(city_objects)
  city_ids = city_result.pluck('id')
  cities.zip(city_ids).to_h # Contains hash { 'city name' => city_id, ... }
end

def insert_services
  service_names = Service::SERVICES
  service_objects = service_names.map { |service| { name: service } }
  service_result = Service.insert_all(service_objects)
  service_ids = service_result.pluck('id')
  service_names.zip(service_ids).to_h # Contains hash { 'service name' => service_id, ... }
end

def insert_buses(buses)
  bus_objects = buses.map { |bus| bus.slice('number', 'model') }
  bus_result = Bus.insert_all(bus_objects)
  bus_ids = bus_result.pluck('id')
  bus_hash_indexes = buses.map { |bus| bus_index(bus) }
  bus_hash_indexes.zip(bus_ids).to_h # Contains hash { "#{bus_model}_#{bus_number}" => bus_id, ... }
end

def bus_index(bus)
  "#{bus['model']}_#{bus['number']}"
end

def insert_bus_service_relations(buses, buses_index, services_index)
  bus_service_pairs = []
  buses.each do |bus|
    bus_id = buses_index[bus_index(bus)]
    bus['services'].each { |service| bus_service_pairs << { bus_id:, service_id: services_index[service] } }
  end

  BusesService.insert_all(bus_service_pairs)
end
