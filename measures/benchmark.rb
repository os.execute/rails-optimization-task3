# frozen_string_literal: true

require_relative './init'

file = 'fixtures/small.json'
result = Benchmark::Perf.cpu { call_task(file) }

puts "Import of #{file} took #{result.avg} s"
