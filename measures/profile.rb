# frozen_string_literal: true

require_relative './init'

file = 'fixtures/example.json'

RubyProf.measure_mode = RubyProf::WALL_TIME
profile = RubyProf.profile do
  call_task(file)
end

printer = RubyProf::MultiPrinter.new(profile, %I[flat graph_html stack])
printer.print(path: './profile', profile: 'work')
