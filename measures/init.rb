# frozen_string_literal: true

ENV['RAILS_ENV'] ||= 'test'
require_relative '../config/environment'

Rails.application.load_tasks

def call_task(file)
  Rake::Task['reload_json'].invoke(file)
end
