source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '3.1.2'

gem 'rails'
gem 'pg'
gem 'puma'
gem 'bootsnap', require: false

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
  gem 'pry'

  # Analyze postgres queries
  gem 'pghero'
  gem 'pg_query'

  # Rack-mini-profiler
  gem 'rack-mini-profiler'
  gem 'memory_profiler'
  gem 'stackprof'
end

group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console'
  gem 'listen'

  # N + 1 Preventing
  gem 'bullet'
end

group :test do
  gem 'benchmark'
  gem 'rspec-benchmark'
  gem 'rspec-rails'
  gem 'ruby-prof'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
