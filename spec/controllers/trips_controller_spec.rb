# frozen_string_literal: true

def cleanup_html(html)
  html.split("\n").reject(&:empty?).map(&:lstrip).join("\n")
end

describe TripsController do
  render_views

  before do
    Rake::Task['reload_json'].invoke('fixtures/small.json')
  end

  after(:each) do
    Rake::Task['reload_json'].reenable
  end

  it 'is rendering as it should' do
    params = { from: 'Самара', to: 'Москва' }
    get :index, params: params
    expect(cleanup_html(response.body)).to eq(cleanup_html(File.read('./spec/fixtures/small_output.html')))
    # File.write('./spec/fixtures/small_output.html', response.body)
  end
end
