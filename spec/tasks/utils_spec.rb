# frozen_string_literal: true

Rails.application.load_tasks

describe 'reload_json task' do
  after(:each) do
    Rake::Task['reload_json'].reenable
  end

  context 'performance regression' do
    let(:small_file) { 'fixtures/small.json' }
    let(:previous_benchmark) { 0.36 }

    it 'is not taking more time than before' do
      measure = Benchmark::Perf.cpu { Rake::Task['reload_json'].invoke(small_file) }
      expect(measure.avg).to be < previous_benchmark * 1.1
    end
  end
end
