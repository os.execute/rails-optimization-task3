# Rails optimization case study


I have a json file full of trips data.
Each trip has source city, destination, has some data on it's own, and has a bus.
Bus has name, model and many-to-many association with services. 
In json all data is nested into single object.

Initially import of the `'fixtures/small.json'` (1k trips) took around **10** seconds.

Our budget was **1** minute for the import of `'fixtures/large.json'` (100k trips)

## Part I. Rake task optimizations.

### Benchmarks and profilers
Added `benchmakr-perf` benchmark to measure how much time it takes to perform a task. Protected from bad decisions leading to regression with performance regression test. 

Added `ruby-prof` profiler, but it's output is almost useless even on the 3kb file since stack is TOO DEEP and outputting `stack` html is over 30mb.



### Optimizations and refactoring
Currently in each iteration over Trip object we execute query to create or select city (twice), create or select bus, create or select bus services

So what I did is rewrited code to store object data in memory, then for cities and buses I do bulk insert (had to update Rails version from 5.2 to 6.1+ (newest 7.0)),
take ids of newly created objects and store them in hashes, where id is a value and key is object's unique property (city name, bus model+number, service name)

Then I create many-to-many relations between buses and services, had to add intermediate model for that, but db structure stayed the same

And finally there happens bulk insert of Trips, using ids of previously inserted cities and buses

Execution time lowered from **10** seconds for 1k trips to **0.36** seconds.

Execution time of `'fixtures/large.json'` (100k trips) took **7** seconds, which is not exceeding our budget any more.

### Regression test
Regression test was implemented to protect functionality of import code from changing.
It compares current view output with the output created by original code
Some non-breaking tweaks had to be done to the view for test consistency related to service sorting.


## Part II. Controller optimizations.

The page with 1k trips loads for 20 seconds. That is bad. We need to optimize it.

Using the `bullet` gem and just watching the logs I found out that there is a separate SQL query for every trip's bus and then it's services. `rack-mini-profiler` showed that more than 600 sql queries being run (some buses are cached) for buses and than alot of queries for separate

I optimized it by using `.includes`, thus reducing SQL requests to 7 (**destination** and **target** city, trip **count**, **trips** itself, **buses**, **buses_services** relation and **services**)

But seems like the loading time was not reduced that much. It loads for 16 seconds now, and it shows like rendering of the partials are taking all the time.

After optimizing how the partials are rendered and nested rendering time took 3+ seconds which is better. Disabling `bullet` lowered rendering time of page with 1k trips to less that 1 second.

